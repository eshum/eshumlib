(module eshumlib *

(import 
  scheme
  (chicken base)
  (chicken port)
  (chicken random)
  (chicken syntax)
  (chicken format)
  (chicken string)
  srfi-1
  srfi-19-core
  srfi-19-io
  srfi-69
  srfi-180
  sha256-primitive message-digest-byte-vector ; http://wiki.call-cc.org/eggref/5/sha2
  icu ; http://wiki.call-cc.org/eggref/5/icu
  )
(import-for-syntax (chicken string) (chicken keyword) srfi-1)


(define (to-json obj) (with-output-to-string (lambda () (json-write obj))))
(define (from-json str) (with-input-from-string str json-read))
(define (alist->json alist) 
  (define (remove-symbols assocs)
    (if (symbol? (cdr assocs)) 
        (cons (car assocs) 
              (symbol->string (cdr assocs))) 
        assocs))
  (to-json (map remove-symbols alist)))
(define (string->sha256 s) (message-digest-string (sha256-primitive) s))
  
(define (randomInt min max) (+ min (pseudo-random-integer (- max min))))
(define (randomUpperChar) (integer->char (randomInt 65 91)))
(define (randomString) 
  (list->string (map (lambda (x) (randomUpperChar)) (iota (randomInt 15 25)))))

(define (string-capitalize s)
  (if (and (string? s) (< 0 (string-length s)))
      (conc (char->upper (car (string->list (substring s 0 1))))
            (substring s 1))
      s))

(define (date->jwt-time date) (string->number (date->string date "~s") 10))
(define (jwt-time->date time) 
  (cond
    [(number? time) (seconds->date time)]
    [(string? time) (jwt-time->date (string->number time))]
    [else (jwt-time->date (->string time))]))
(define (date->formatted-string date) (date->string date "~4")) ; ISO-8601 year-month-day-hour-minute-second-timezone format
(define (jwt/iat) (date->jwt-time (current-date)))
(define (now/formatted) (date->formatted-string (current-date)))
(define (now/date-header) 
  (define now (current-date (utc-timezone-locale)))
  (define daywords (string-capitalize (date->string now "~a")))
  (define day (date->string now "~d"))
  (define month (string-capitalize (date->string now "~b")))
  (define rest (date->string now "~Y ~H:~M:~S GMT"))
  (sprintf "~a, ~a ~a ~a" daywords day month rest))

(define (assoc-value key alist)
  (define result (assoc key alist))
  (if result (cdr result) #f))

;;; use get-keyword (from chicken keyword) for getting the value
(define (filter-keyword xs . keywords)
  (define (iter lst skip acc)
    (if (null? lst)
        (reverse acc)
        (if (< 0 skip)
            (iter (cdr lst) (- skip 1) acc)
            (if (member (car lst) keywords)
                (iter (cdr lst) 1 acc)
                (iter (cdr lst) 0 (cons (car lst) acc))))))
  (iter xs 0 '()))
  
(define (invert-filter-keyword xs . keywords)
  (define (iter lst shouldadd acc)
    (if (null? lst)
        (reverse acc)
        (if shouldadd
            (iter (cdr lst) #f (cons (car lst) acc))
            (if (member (car lst) keywords)
                (iter (cdr lst) #t (cons (car lst) acc))
                (iter (cdr lst) #f acc)))))
  (iter xs #f '()))

(define (filter-alist alist . keys)
  (define (iter xs acc)
    (cond
      [(null? xs) (reverse acc)]
      [(member (caar xs) keys) (iter (cdr xs) acc)]
      [else (iter (cdr xs) (cons (car xs) acc))]))
  (iter alist '()))
  
(define (invert-filter-alist alist . keys)
  (define (iter xs acc)
    (cond
      [(null? xs) (reverse acc)]
      [(member (caar xs) keys) (iter (cdr xs) (cons (car xs) acc))]
      [else (iter (cdr xs) acc)]))
  (iter alist '()))

;; Remember to only use it with pure functions!
(define (memoize function) 
  (let ([table (make-hash-table)]) 
    (lambda args 
      (if (hash-table-exists? table args)
          (hash-table-ref table args)
          (let ([result (apply function args)])
            (hash-table-set! table args result)
            result)))))

)