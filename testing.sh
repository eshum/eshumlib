#!/bin/bash

while inotifywait -q --event close_write --format '%w' ./*; do 
  csi test
  ret=$?
  # Play a soun to indicate failure
  if [ $ret -ne 0 ]; then 
    for i in {1..10}; do 
      printf '\7'
      sleep 0.12 
    done
  fi 
done