(import (chicken format)
        srfi-1 srfi-19-core srfi-19-io srfi-69 srfi-180 sha2 icu
        test
        eshumlib)


(test-group "filter-keyword"
  (test "Returns same list if keyword not in input"
    '(item1: value1 item2: value2)
    (filter-keyword 
      '(item1: value1 item2: value2) 
      filtered:))

  (test "Returns empty list if input is empty"
    '()
    (filter-keyword '() filtered:))

  (test "Filters out value"
    '(item1: value1 item2: value2)
    (filter-keyword 
      '(item1: value1 filtered: notshown item2: value2) 
      filtered:))
      
  (test "Filters out multiple values"
    '(item1: value1 item2: value2)
    (filter-keyword 
      '(item1: value1 filtered: notshown item2: value2 morefiltered: notshown) 
      filtered: morefiltered:))
)

(test-group "invert-filter-keyword"
  (test "Returns empty list if keyword not in input"
    '()
    (invert-filter-keyword 
      '(item1: value1 item2: value2) 
      filtered:))

  (test "Returns empty list if input is empty"
    '()
    (invert-filter-keyword '() filtered:))

  (test "Includes value"
    '(filtered: notshown)
    (invert-filter-keyword 
      '(item1: value1 filtered: notshown item2: value2) 
      filtered:))
      
  (test "Includes multiple values"
    '(filtered: notshown morefiltered: notshown)
    (invert-filter-keyword 
      '(item1: value1 filtered: notshown item2: value2 morefiltered: notshown) 
      filtered: morefiltered:))
)

(test-group "filter-alist"
  (test '() (filter-alist '()))
  (test '((a . 1)) (filter-alist '((a . 1)) 'nonexistant))
  (test '((a . 1)) (filter-alist '((a . 1) (b . 2)) 'b))
  (test '((a . 1) (b . 2)) (filter-alist '((a . 1) (b . 2) (c . 3)) 'c))
  (test '((c . 3)) (filter-alist '((a . 1) (b . 2) (c . 3)) 'a 'b))
)

(test-group "invert-filter-alist"
  (test '() (invert-filter-alist '()))
  (test '() (invert-filter-alist '((a . 1)) 'nonexistant))
  (test '((b . 2)) (invert-filter-alist '((a . 1) (b . 2)) 'b))
  (test '((a . 1) (b . 2)) (invert-filter-alist '((a . 1) (b . 2) (c . 3)) 'a 'b))
)

(test-exit)
